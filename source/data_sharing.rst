======================
Data Sharing Platforms
======================

.. toctree::	
	:caption: Table of Contents
	:maxdepth: 2
	
	data_sharing

Overview of Recommended Data Sharing Platforms
==============================================
.. image:: ../Images/recommended.png

Compute Canada
==============
On Compute Canada clusters, Home spaces are unique to individual users while 
the Project space is shared by a research group. Both are backed up and are not purged.
Quotas are exclusive to each cluster and do not extend to the other clusters.

.. csv-table:: Storage Quotas
   :header: "Cluster", "Home Space", "Project Space"
   :widths: 10, 15, 15

   "Cedar", 50 GB and 0.5 M files per user, 1 TB and 5 M files per group
   "Graham", 50 GB and 0.5 M files per user, 1 TB and 0.5 M files per group
   "Béluga", 50 GB and 0.5 M files per user, 1 TB and 0.5 M files per group


This means they can be used to share files to Compute Canada users. If a Globus subsription 
is purchased, files can be shared with non Compute Canada users as well. 
`Learn more here <https://docs.globus.org/how-to/share-files/>`_.


Sharing files with Compute Canada Users
---------------------------------------
This section contains quick references. For detailed documentation and 
instructions, refer to these `docs <https://docs.computecanada.ca/wiki/Sharing_data>`_.

.. Note:: 
   * You must know the Compute Canada username of the person you want to share with.
   * You must also own the files/directories that you intend on sharing. Use `chown <https://linux.die.net/man/1/chown>`_ to change file owners and groups.
   * Learn more about UNIX file permsissons `here <https://en.wikipedia.org/wiki/File_system_permissions>`_.

Internal Sharing
~~~~~~~~~~~~~~~~
To share files within your own research group, refer to these 
`docs <https://docs.computecanada.ca/wiki/Sharing_data#Filesystem_permissions>`_.

External Sharing
~~~~~~~~~~~~~~~~
To share files with other Compute Canada users, refer to to these 
`docs <https://docs.computecanada.ca/wiki/Sharing_data#Access_control_lists_.28ACLs.29>`_

Quick Reference
^^^^^^^^^^^^^^^
Sharing a Single file
.....................
If you are logged in as ``<user>`` on compute cluster ``<server>`` and want to share a 
generic file, e.g. ``<data.zip>`` with another user ``<other_user>``, 

.. code-block:: bash
   
   [<user>@<server>]$ setfacl -m u:<other_user>:rx <data.zip>
   
   # substitute the variables, but don't include the <>'s
   #                            `u` to identify other users to share with
   #                                           'rx' - give the user the rights to:
   #                                           `r` - read 
   #                                            'x' - execute


Sharing a folder
~~~~~~~~~~~~~~~~
For most use cases, it is more practical to share one or more folders and give 
different groups access to them using Access Control Lists (ACL).

.. note:: 
   * You must have a group created in `CCDB <https://ccdb.computecanada.ca/services/>`_
   * You must own the directory you want to share
   * All parent directories of the directory you want to share must have public execute properties
   
   If you do not meet these requirements, consult the 
   `docs <https://docs.computecanada.ca/wiki/Sharing_data#Access_control_lists_.28ACLs.29>`_.

If you are logged in as ``<user>`` on compute cluster ``<server>`` and want to share a folder 
``<shared_data>`` in some project or home space to a group ``<group_name>``

.. code-block:: bash
   
   # make any file inside the folder inherit the same ACL rules (for new files)
   [<user>@<server>]$ setfacl -d -m g:<group_name>:rwx /home/<user>/projects/<def/rrg-PI>/<shared_data>

   # make any file inside the folder inherit the same ACL rules (for existing files)
   [<user>@<server>]$ setfacl -d -m g:<group_name>:rwx /home/<user>/projects/<def/rrg-PI>/<shared_data>  
 
Members can be added to the a group through `CCDB <https://ccdb.computecanada.ca/services/>`_.

Open Science Framework
======================
Open Science Framework (OSF) is a free and open source cloud-based data management 
system developed by the Center for Open Science.

The following introduction is based on the OSF Guides.

Storage and Backup
------------------
OSF uses Google Cloud for active and archival storage, and Amazon Glacier as a backup location. 

Location
--------
The U.S. is OSF’s default storage location. A variety of storage locations are available,
including Canada (Montreal). The global (default) storage location can be changed and will 
be applied to new projects and components. Each project can also have its own storage location.

Size limits
-----------
There is no limit on storage per project and no cap on the amount of OSF Storage per user. 

Direct upload of individual files to OSF Storage has a 5GB limit. However, third-party add-ons
like Dropbox can be used to integrate different services already in use and connect them to OSF
to allow access to existing data/materials. There is no limit for the amount of storage used
across add-ons. See the Add-ons section for more information.

Add-ons
-------
Citation add-ons
~~~~~~~~~~~~~~~~
There are two reference managers supported by OSF:

1. `Mendeley <https://help.osf.io/hc/en-us/articles/360019929893-Connect-Mendeley-to-a-Project>`_

2. `Zotero <https://help.osf.io/hc/en-us/articles/360019929913-Connect-Zotero-to-a-Project>`_

Storage add-ons
~~~~~~~~~~~~~~~
If data needed for a project already exists in one of the services below, 
it can be connected to OSF rather than transferring it. 
This feature is also useful if a file exceeds the 5GB limit for upload. 
Note that files connected to third-party storage add-ons are not stored or backed up on OSF.

This page provides a `list of available storage add-ons in OSF 
<https://help.osf.io/hc/en-us/sections/360003623833-Storage-add-ons>`_, which includes platforms like 
Amazon S3, ownCloud, Dataverse, GitHub, Bitbucket and GitLab.

.. warning::
	OSF does not store or back up content within an add-on. Ensure that each individual
	add-on being used complies with `UBC's Information Security Standard #03: Transmission
	and Sharing og UBC Electronic Information <https://cio.ubc.ca/sites/cio.ubc.ca/files/documents/standards/Std%2003%20Transmission%20and%20Sharing%20of%20UBC%20Electronic%20Information.pdf>`_.
	
Collaboration
-------------

Within a lab
~~~~~~~~~~~~

**Contributors**

OSF projects and components are private by default. 
Members of the lab can be added as “Contributors” to projects. There are two types of contributors:

1. Bibliographic (displayed in contributor list and in project citations)
2. Non-bibliographic

Each component within a project can have its own set of contributors.

**User Permissions**

A contributor can be given three levels of permissions: 

1. Administrator
2. Read + Write
3. Read only

Each contributor can be granted different levels of permissions across components. 

**Checkouts**

OSF has a checkout feature that prevents multiple contributors from editing the same file at once.     

Across labs
~~~~~~~~~~~

**Access requests**

Researchers can request access to both private and public projects, 
which means they can be added as a contributor and given a level of permission. 

**View-only (Projects)**

For peer review, presentations, or data sharing, a view-only link can be created
for a project. The contributor list can be anonymized for blind peer review and
only selected components will be visible via the link. 

**Quick Files**

A single file can be shared independently from a project using the Quick File feature, 
which allows files to be publicly searchable and accessible on the profile page.

Public Access
~~~~~~~~~~~~~

**DOIs**

DOIs can be created for projects but OSF does not currently support DOI versioning.

**Citations**

OSF generates citations (APA, MLA, Chicago, or custom) automatically for every
project and component.

**Licensing**

A license can be added to a project either by choosing from a list provided by
OSF or uploading your own. Components will have the same license as the
top-level project by default but they can also be licensed individually.

Version Control
---------------
OSF has built-in version control and provides access to previous versions of
files, including those stored on add-ons.

Registration
~~~~~~~~~~~~

A registration is a time-stamped copy of an OSF project that cannot be edited or
deleted. This feature is useful for archiving and to capture and preserve significant
moments in the research process (i.e. before submission for peer review, etc.).

A registration can be withdrawn, which means the project contents will be removed but its 
basic metadata will be maintained. 
All registrations will be made public, which can be done immediately or embargoed for up to 4 years. 

Scholars Portal Dataverse
=========================
Scholars Portal Dataverse is a publicly accessible secure Canadian data repository 
provided by Scholars Portal on behalf of the `Ontario Council of University 
Libraries (OCUL) <https://ocul.on.ca/>`_ and other participating institutions. Dataverse can be used by 
affiliated researchers to deposit, share, and archive research data.

.. todo::

   citation: https://learn.scholarsportal.info/all-guides/dataverse/

We are pleased to announced that we have set up 
a `dataverse for the cluster <https://dataverse.scholarsportal.info/dataverse/UBC_BrainCircuits>`_, 
under which we can set up dataverses for individual labs that are administered by PIs. 

Contact Jeffrey Ledue or Glaynel Alejo to set up a dataverse for your lab.


Storage and Backup
------------------

Location
~~~~~~~~
The Scholars Portal Dataverse is hosted in Canada at the UofT Libraries. 
Although institutional Datavarses can be set up, the data is still stored at this 
data center.

Size Limits
~~~~~~~~~~~
The limit for an individual file upload is 2.5 GB
There are also other considerations to be made when making uploads:

1. When .zip or .tar archives are uploaded, they can be:
 - Unpacked, with loss of hierarchy and file organisation. They recommend having fewer than 500 files in each archive due to processing and user experience concerns.
 - Packed, maintaining hierarchy and file organisation. To achieve, this, the archive must be zipped or tarballed.

2. Tabular data files (Excel, SPSS, R Data, CSV) should be less than 500MB in size as the processing that takes place at upload time will make the process slow. 

For larger datasets, dataverse support can be contacted at  dataverse@scholarsportal.info. 
However, the recommended next step is to use, FRDR.

User Permissions
----------------
Users can be granted the following tiered access and permissions:

1. Admin: A person who has all permissions for dataverses, datasets, and files.
2. Contributor: For datasets, a person who can edit License + Terms, and then submit them for review.
3. Curator: For datasets, a person who can edit License + Terms, edit Permissions, and publish datasets.
4. Dataset Creator: A person who can add datasets within a dataverse.
5. Dataverse + Dataset Creator: A person who can add subdataverses and datasets within a dataverse.
6. Dataverse Creator: A person who can add subdataverses within a dataverse.
7. File Downloader: A person who can download a published file.
8. Member: A person who can view both unpublished dataverses and datasets.


Making Changes to Datasets
--------------------------
Users with curation persmissions can edit dataset metadata and contents.
Dataverse has built in versioning to ensure data is preserved. 
It is recommended that curators are clearly identified in your labs’ DMP, as well 
as the guidelines and procedures they should abide by.

Dataset Templates
-----------------
Dataverse provides the following templates by default:
1. CC Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
2. CC Attribution-Non-Commercial 4.0 International (CC BY-NC 4.0)
3. CC Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) 
4. CC Attribution 4.0 International (CC BY 4.0)

It is possible to create custom templates. While it is not possible to create custom fields, 
there are large selections of metadata fileds available in the template creator, and the ability 
to create keywords for other particulars.

Labs are encouraged to create standardised templates for their datasets to ensure all required 
metadata are captured.

Federated Research Data Repository (FRDR)
=========================================
`Portage <https://portagenetwork.ca/>`_, `Compute Canada (CC) <https://www.computecanada.ca/>`_ 
and the `Canadian Association of Research Libraries (CARL) <http://www.carl-abrc.ca/about-carl/>`_ 
have collaborated to produce the `Federated Research Data Repository <https://www.frdr.ca/repo/>`_.


Storage and Backup
------------------
Location
~~~~~~~~
From frdr.ca/docs/en/about:

    Data submitted to FRDR is housed on Compute Canada managed infrastructure at the University of Victoria, 
    BC or at the University of Waterloo, ON. Research data submitted to FRDR does not leave Canada.
    The metadata related to datasets is housed in a database the University of Victoria. 
    Most of that metadata is shared with Globus, running on Amazon Web Services services in the USA, 
    to be indexed and made available for discovering datasets. Certain metadata fields, 
    for example, submitter contact information, are not shared with Globus.

Larger uploads are made using Globus. While globus is hosted in the USA on AWS, 
only the public metadata is stored there; the datasets themselves are transferred 
securely between the endpoints so the data does not leave Canada.

Size limits
~~~~~~~~~~~
There is a theoretical dataset limit of 4TB due to the limitation of the Archivematica
data preservation system. There has not been further comment on whether they will 
impose their own upload size limit.

However, they do have limited resources so they may impose restrictions during curation 
if datasets of unreasonable magnitude are uploaded.

Changes
-------
Only authorised curators can make changes to submitted data. 
A request must be made via email in case any changes need to be made.

Curation 
~~~~~~~~
Curators will perform tasks like:
* Checking metadata record for completeness
* Linking DOIs for related publications 
* Validation of data, for instance, flagging tabular null data with no explanation, corrupt files
* Checking documentation
* Checking for copyright and ethical violations

They typically take up to 48 hours to complete this process, after which it takes 15 minutes to 
complete DOI registration, and a further 2 hours for the dataset to appear for download 
in the FRDR portal.

Data Sharing and Collaboration
------------------------------
FRDR includes search functionality for its own datasets and datasets 
that it harvests from other sources such as the Scholars Portal Dataverse. 
Users can search for/deposit datasets using the online web interface or by using an API. 
Note that while depositing data requires authorisation, anybody can search for datasets.
FRDR is also format agnostic, and allows users to manage the dataset file hierarchy. 
It also support embargos. They also issue DOIs for all deposited datasets.
Other features include data integrity checks using checksums, curation and upload authentication.
Through the use of Globus, FRDR also enables secure transfer of large datasets with the ability 
to make asynchronous and resumable transfers that are automatically managed.

Bespoke solutions
==============
In some cases, a custom data sharing solution is required, for instance, due to the need to host 
a persistent database, a website or a highly specialised analysis environment.

Compute Canada Cloud
--------------------
To get a Cloud project, the PI must have an active cloud resource allocation as part of their 
Resource Allocation Competition (RAC) allocation. Users sponsored under such a PI can request 
access to a Cloud project, and have to be granted access by their PI.
Read the documentation `here <https://docs.computecanada.ca/wiki/Cloud>`_

EduCloud
--------
EduCloud can be accessed on-demand and may be faster and easier to set up than on Compute Canada 
Cloud, but costs money. You must `apply for an EduCloud account <https://web.it.ubc.ca/forms/systems/>`_ 
before you can use the service.

From the `UBC Website <https://it.ubc.ca/services/web-servers-storage/educloud-server-service>`_ :
    EduCloud Server is a self-managed, private higher education cloud server service that provides simple and secure virtual data centre access to provision, manage and utilize servers at a fraction of the cost of implementing physical servers. EduCloud Server is designed to provide functionality which allows self-management and self-deployment.  EduCloud Server is intended to provide the same convenience and cost effectiveness as other cloud services, while meeting all provincial privacy requirements under the FIPPA legislation.
    EduCloud Server provides the ability to easily self-deploy virtual machines from templates. With this service, you can create and delete virtual machines, and change resource allocation all within minutes.  Managed using a web portal, you have the flexibility of managing your own environment at any time, from anywhere.

The costs of deploying the virtual machines can be found `here <https://it.ubc.ca/services/web-servers-storage/educloud-server-service/pricing-model>`_.
Find documentation and other information `here <https://it.ubc.ca/services/web-servers-storage/educloud-server-service>`_.

Overview of Other Suitable Data Sharing Platforms
=================================================

.. image:: ../Images/other.png
    
Zenodo
======
`Zenodo <https://zenodo.org/>`_ 
is an open access data repository operated by CERN [1]_ . 
It is hosted on the high-performance infrastructure operated for the needs of high-energy physics
[#]_ .

Location
--------
Zenodo is hosted by CERN, which has an experimental programme defined for at least the next 20 years. 
The technical infrastructure is located on CERN’s premises on CERN’s EOS service in an 18 PB disk cluster, 
with two copies kept of each file on difference disk servers. 
Checksums are used to detect changes and enable automatic corruption detection and recovery.

Size Limits
-----------
The maximum they accept per dataset is 50GB, with the ability to upload an unlimited amount of datasets. 
If larger files are to be uploaded, they can be contacted to discuss further. 

Changes
-------
If changes need to be made to the dataset, due to reasons such as typos or accidental omissions,
and the upload was made within a week, a contact request can be made with Zenodo to make the modification.
Once the record has been published, you cannot change the files on record.

Collaboration and Data Sharing
------------------------------
You can create collections and control what uploads can be made to them.
There are no limitations to what type of data is uploaded any file format
and any results can be submitted. Zenodo  also assigns all publicly accessible
records a DOI to make the upload more accessible and citable. It provides DOI versioning,
such that uploads have a DOI representing a specific version of the dataset or a DOI
representing all the versions. This allows for specific citations to be made.
Github integration is also included, which allows users to track each release.
There are also over 400 licenses available to choose from to assign to your datasets.

Git-Annex
=========
`Git-Annex <https://git-annex.branchable.com/>`_ uses git to create an annex, which presents 
files to the user in a single directory structure, even though the individual files are distributed across 
multiple locations. It can also be confiogured to create a number of copies of a file distributed across 
different annexes. This enables users to remove a local copy while ensuring redundancies are available on 
other storage locations. It is also able to synchronise files across redundancies. 
File versions can be uniquely tracked and referenced using git changeset hashes.

It also comes with a `webapp interface <http://git-annex.branchable.com/assistant/>`_.

.. Note::
   Git-Annex is a powerful tool but requires knowledge of git, UNIX command line and careful scripting to use 
   effectively.

Use Case Demo: Syncing files with Git-Annex using Linux CLI
-----------------------------------------------------------
The following demo was tested on Ubuntu 18.04 LTS

.. note::
   
   sudo priviledges are required to install git-annex

Install the `Git-Annex <http://neuro.debian.net/install_pkg.html?p=git-annex-standalone/>`_ from NeuroDebian.

Create a repository in a location of your preference

.. code-block:: bash
   
   $ mkdir annex
   $ cd annex
   $ git init
   Initialized empty Git repository in /home/user/annex/.git/
   $ git annex init
   init ok

Add file to the repository

.. code-block:: bash

   $ cd /home/user/annex/
   $ cp ~/Pictures/121406.jpg ./
   $ git annex add .
   add 121406.jpg ok
   (recording state in git...)
   $ git commit -a -m added
   [master (root-commit) 1259e1c] added
   1 file changed, 1 insertion(+)
   create mode 120000 121406.jpg

Add a remote, in this case, an external hard drive called 'My Passport'

.. code-block:: bash

   $ cd /media/user/USB\ DISK/
   $ git clone /home/user/annex/
   Cloning into 'annex'...
   done.
   $ cd annex
   # get the desktop and the hard drive to get to know each other
   $ git annex init
   $ git remote add desktop /home/user/annex
   $ cd /home/user/annex
   $ git remote add harddrive1 /media/user/My\ Passport/annex

Now let's add a bunch of files to the Desktop's Annex

.. code-block:: bash
   $ cd /home/user/annex/
   $ cp ~/Pictures/121419.jpg ~/Pictures/121421.jpg ./
   $ git annex add .
   add 121419.jpg ok
   add 121421.jpg ok
   (recording state in git...)
   $ git commit -a -m added
   [master 8a68299] added
   2 files changed, 2 insertions(+)
   create mode 120000 121419.jpg
   create mode 120000 121421.jpg

And now let's add a bunch of other files to the Hard Drive's Annex

.. code-block:: bash
   $ cd /media/user/My\ Passport/annex
   $ cp ~/Pictures/121415.jpg ~/Pictures/121420.jpg ./
   $ git annex add .
   add 121419.jpg ok
   add 121421.jpg ok
   (recording state in git...)
   $ git commit -a -m added
   [master 8a68299] added
   2 files changed, 2 insertions(+)
   create mode 120000 121419.jpg
   create mode 120000 121421.jpg

Looking at the contents of the Desktop annex, we see the following:

.. code-block:: bash

  $ ls
  121406.jpg  121419.jpg  121421.jpg

Looking at the contents of the Hard drive annex, we see the following:

.. code-block:: bash

  $ ls
  121406.jpg  121415.jpg  121420.jpg

Now we need to sync the files and make sure our annexes have the same contents
  
.. code-block:: bash  
  $ cd /media/user/My\ Passport/annex
  $ git annex sync desktop
  $ git annex get .
  $ cd /home/user/annex/
  $ git annex sync harddrive1
  $ git annex get .

Now, looking at the contents of the Desktop annex, we see the following:

.. code-block:: bash

  /home/user/annex$ ls
  121406.jpg  121415.jpg  121419.jpg  121420.jpg  121421.jpg

And also when looking at the contents of the Hard drive annex, we see the following:

.. code-block:: bash

  /media/user/My Passport/annex$ ls
  121406.jpg  121415.jpg  121419.jpg  121420.jpg  121421.jpg

This can be automated as a cron job that syncs your files with your backups in regular intervals

Refer to the `documentation <http://git-annex.branchable.com/walkthrough/#index21h2>`_
to learn more about setting up ssh remotes, removing and transferring files and troubleshooting.

DataLad
=======
DataLad is a solution that provides decentralised access to data from open data annexes.
It provides a command line interface to interact with the technologies it is built on,
such as git and Git-Annex to provide functionality such as dataset searches, dataset nesting,
dataset collections and git annex file management. This makes it a good candidate for 
further extensibility using a web interface.

DataLad also comes with support for metadata, with a search command that enables metadata 
queries via a flexible query language.

Use Case Demo: DataLad on Ubuntu
--------------------------------
.. note::
   
   sudo priviledges are required

Install the `Git-Annex <http://neuro.debian.net/install_pkg.html?p=git-annex-standalone/>`_ from NeuroDebian.
Install DataLad

.. code-block:: bash

   $ sudo apt-get install datalad


.. rubric:: Footnotes

.. [1] https://about.zenodo.org/infrastructure
.. [#] http://eos.web.cern.ch/
