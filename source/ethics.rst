==============================================
Data Sharing: Legal and Ethical Considerations
==============================================

.. toctree::	
	:caption: Table of Contents
	:maxdepth: 3
	
	ethics

Copyrights
==========
Depositors of data must ensure that no copyrights are infringed and that attributions 
and acknowledgements are correctly made.
Labs producing secondary data and working with proprietary software should create clear 
guidelines for dealing with copyright issues in their DMPs.

.. note::
   Some platforms like FRDR perform copyright checks during their curation process. 
   Other platforms do not offer this safety net and should be used with caution.

Animal Data
===========
All animal data must be collected and shared ethically. 
This includes: 

1. Strict compliance with animal care rules, regulations and guidelines
2. Considering access rights to graphic or sensitive datasets
3. Considering whether personell attribution may pose risks to thier security
4. Developing an ethics approval procedure as part of each lab's data sharing plan to prevent breaches. This is a prudent measure if data is shared on a platform where curation rights are not under our control.

The `UBC Animal Care Commitee <https://animalcare.ubc.ca/about-acup/contact-us#ACC-Contact>`_ should be consulted when developing DMPs.

Human Data
==========

.. todo::
   Human data sharing ethics

1. Deidentification
2. Large enough to prevent identification
3. Permission and patient approval
4. 
