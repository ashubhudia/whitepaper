.. UBC Dynamic Brain Circits Cluster Whitepaper documentation master file, created by
   sphinx-quickstart on Tue Jun 18 10:48:04 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the UBC Dynamic Brain Circits Cluster Whitepaper!
============================================================

.. todolist::


10,000 Feet Overview
--------------------
.. todo::
   Write Abstract

   This whitepaper is intended as a guide to PIs and cluster members on best practices in data 
   management and data sharing. It also aims to provide information and point to resources that 
   can assist in the development of Data Management Plans.

   While each PI is ultimately responsible for their lad's data manegement and each lab member is 
   responsible for managing their data, we will provide support and have availed resources to the cluster.

   These include:
   * Data sharing repositories: Dataverse, Federated Research Data Repository, Open Science Framework
   * JupyterHub: Interactive coding and analysis notebook platform
   * GitHub Team

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Data Archival <archival>
   alder
   Data Sharing Platforms <data_sharing>
   workflows
   Data Sharing Ethics <ethics>
   version_control
   data_standard
   compute_canada
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
